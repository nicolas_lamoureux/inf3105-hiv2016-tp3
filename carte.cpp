/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#include <cstdio>
#include <map>
#include <set>
#include <cctype>
#include <istream>
#include <algorithm>
#include "carte.h"
#include "disjointset.h"

istream &operator>>(istream &is, Carte &carte) {
    std::string ligne;

    while (is >> ligne && ligne != "---") {
        carte.points.push_back(ligne);
        is >> ligne >> ligne; // Skip the coordinates and delimiter
    } // skip everything up to the --- line

    while (!is.eof() && is.peek() != EOF) {
        Rue rue;
        is >> rue;

        carte.rues.push(rue);
    }

    return is;
}

void Carte::getMaximumSpanningTree(priority_queue<Rue, vector<Rue>, greater<Rue> > &mst) {
    // The disjoint set contains all the trees and union trees if the two given nodes are not in the same tree
    DisjointSet *ds = new DisjointSet();

    // Counter to know when all the nodes are in the tree
    // This is used to abort the algorithm early
    unsigned int nbRue = 0;

    // Create the initial trees in the Disjoint Set
    for (vector<string>::iterator it = this->points.begin(); it != this->points.end(); ++it) {
      ds->makeSet(*it);
    }

    // For each Rue as long as the required number of Rue is reached
    while (!this->rues.empty() && nbRue < this->points.size() - 1) {
        Rue rue = this->rues.top();
        this->rues.pop();

        // Try to union the trees
        if (ds->unionSet(rue.pointUn, rue.pointDeux)) {
            // Update the mst if successful
            nbRue++;
            mst.push(rue);
        }
    }

    delete ds;
}
