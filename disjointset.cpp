/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#include "disjointset.h"

Node::Node(string name) {
    this->name = name;
    this->rank = 0;
    this->parent = this;
}

DisjointSet::DisjointSet() { }

DisjointSet::~DisjointSet() {
    for (unordered_map<string, Node*>::iterator it = this->forest.begin(); it != this->forest.end(); ++it) {
        delete (*it).second;
    }
}

void DisjointSet::makeSet(string name) {
    Node *node = new Node(name);
    this->forest[name] = node;
}
bool DisjointSet::unionSet(string name1, string name2) {
    if (name1 == name2) return false;

    Node *root1 = this->find(name1);
    Node *root2 = this->find(name2);

    if (root1 == root2) return false;

    if (root1->rank < root2->rank) {
        root1->parent = root2;
    } else if (root1->rank > root2->rank) {
        root2->parent = root1;
    } else {
        root2->parent = root1;
        root1->rank++;
    }

    return true;
}

Node* DisjointSet::find(string name) {
    unordered_map<string, Node*>::iterator it = this->forest.find(name);
    if (it != this->forest.end())
        return find((*it).second);

    return NULL;
}

Node* DisjointSet::find(Node *node) {
    if (node->parent != node) {
        node->parent = this->find(node->parent);
    }
    return node->parent;
}
