/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#if !defined(_DISJOINTSET__H_)
#define _DISJOINTSET__H_

#include <string>
#include <unordered_map>

using namespace std;

class Node {
    string name;
    int rank;
    Node *parent;

public:
    Node(string name);

    friend class DisjointSet;
};

class DisjointSet {
public:
    DisjointSet();

    ~DisjointSet();

    /**
     * Create a tree for the given name
     * @param name The name used to create the tree
     */
    void makeSet(string name);
    /**
     * Return the parent node of the tree for a given name.
     * @param name The name used to find the parent node.
     * @return The parent node of the tree for a given name.
     */
    Node *find(string name);
    /**
     * Union the tree for name1 and the tree for name2 if they are not the same.
     * @param name1 The identifier for the first tree.
     * @param name2 The identifier for the second tree.
     * @return True if the union occured, else false.
     */
    bool unionSet(string name1, string name2);

private:
    unordered_map<string, Node*> forest;
    Node *find(Node *node);
};

#endif
