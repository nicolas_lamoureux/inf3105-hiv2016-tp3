/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#if !defined(_CARTE__H_)
#define _CARTE__H_

#include <queue>
#include "rue.h"

using namespace std;

class Carte {
public:
  /**
   * Return the ARMax of the Carte.
   * @return The ARMax of the Carte.
   */
  void getMaximumSpanningTree(priority_queue<Rue, vector<Rue>, greater<Rue> > &mst);

private:
  priority_queue<Rue> rues;
  vector<string> points;

  friend istream &operator>>(istream &is, Carte &carte);
};

#endif

