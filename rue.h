/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#if !defined(_RUE__H_)
#define _RUE__H_

#include <iostream>

using namespace std;

class Rue {
  public:
    Rue();

    std::string nom;
    std::string pointUn, pointDeux;
    int poids;

  private:
    friend std::ostream& operator << (std::ostream&, const Rue&);
    friend std::istream& operator >> (std::istream&, Rue&);
};

bool operator<(const Rue &s1, const Rue &s2);
bool operator>(const Rue &s1, const Rue &s2);

#endif
