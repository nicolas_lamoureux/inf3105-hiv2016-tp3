# Makefile pour INF3105 / TP3

#OPTIONS = -Wall           # option standard
#OPTIONS = -g -O0 -Wall    # pour rouler dans gdb
OPTIONS = -O3 -Wall -std=c++11       # pour optimiser

#
all : tp3

tp3 : tp3.cpp carte.o rue.o disjointset.o
	g++ ${OPTIONS} -o tp3 tp3.cpp rue.o carte.o disjointset.o

carte.o : carte.h carte.cpp
	g++ ${OPTIONS} -c -o carte.o carte.cpp

rue.o : rue.h rue.cpp
	g++ ${OPTIONS} -c -o rue.o rue.cpp

disjointset.o : disjointset.h disjointset.cpp
	g++ ${OPTIONS} -c -o disjointset.o disjointset.cpp

clean:
	rm -rf tp3 *~ *.o

