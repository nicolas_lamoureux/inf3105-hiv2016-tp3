/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
*/

#include <fstream>
#include <iostream>
#include <queue>
#include "carte.h"

using namespace std;

/**
 * Treat the received Carte and output the ARMax and the total value of the ARMax.
 * @param carte The Carte used to calculate the ARMax
 */
void tp3(Carte& carte) {
    priority_queue<Rue, vector<Rue>, greater<Rue> > armax;
    carte.getMaximumSpanningTree(armax);

    int total = 0;
    while (!armax.empty()) {
        Rue rue = armax.top();
        cout << rue;
        armax.pop();
        total += rue.poids;
    }
    cout << "---" << endl << total << endl;
}

int main(int argc, const char** argv) {
    if (argc != 2) {
        cout << "Syntaxe: ./tp3 carte.txt" << endl;
        return -1;
    }

    Carte carte;

    ifstream fichiercarte(argv[1]);
    if (fichiercarte.fail()) {
        cout << "Erreur ouverture du fichier : " << argv[1] << endl;
        return -1;
    }

    fichiercarte >> carte;

    tp3(carte);

    return 0;
}
