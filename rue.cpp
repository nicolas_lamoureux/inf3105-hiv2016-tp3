/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Hiver 2016 TP3
 *
 *  Nicolas Lamoureux - LAMN19109003
 *  Marc-Antoine Sauvé - SAUM13119008
 */

#include <cstdio>
#include <limits>
#include <queue>
#include <sstream>
#include "rue.h"

Rue::Rue() { }

istream& operator >> (istream& is, Rue& rue) {
    if (!is.eof() && is.peek() != EOF) {
        char space;
        is >> rue.nom >> space >> rue.pointUn >> rue.pointDeux >> rue.poids >> space;
    }
    return is;
}

std::ostream& operator << (std::ostream& os, const Rue& pRue) {
    os << pRue.nom << ": " << pRue.pointUn << " " << pRue.pointDeux << " " << pRue.poids << endl;
    return os;
}

bool operator<(const Rue &r1, const Rue &r2) {
    if (r1.poids != r2.poids) return r1.poids < r2.poids;
    if (r1.pointUn != r2.pointUn) return r1.pointUn < r2.pointUn;
    return r1.pointDeux < r2.pointDeux;
}

bool operator>(const Rue &r1, const Rue &r2) {
    return r2 < r1;
}